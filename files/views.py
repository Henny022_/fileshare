import os

from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views import View

from . import models


# Create your views here.

class GetFileView(View):
    def get(self, request, key):
        return render(request, 'get.html')

    def post(self, request, key):
        password = request.POST['password']
        file = models.FileModel.objects.get(_key=key)
        if file.password == password:
            response = HttpResponse(file.file.read(), content_type="application/octet-stream")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file.file.name)
            return response
        raise Http404
